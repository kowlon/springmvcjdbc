package net.codejava.spring.model;

import java.io.IOException;
import java.net.URL;

import javax.swing.JOptionPane;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonRespone   {

	public String getResponeSearch() {
		ObjectMapper mapper = new ObjectMapper();	
		
		try {
					
			JsonNode searchRespone = mapper.readTree(new URL("http://localhost/json/responeSearch.json"));
			JsonNode globalPro = searchRespone.path("global_properties");
			
			JsonNode dateFormat = globalPro.path("date_format");
			JsonNode language = globalPro.path("language");
			JsonNode char_set = globalPro.path("char_set");
			JsonNode api_version = globalPro.path("api_version");
			JOptionPane.showMessageDialog(null, dateFormat.asText()+" "+language.asText()+" "+char_set.asText()+" "+api_version.asText());
		
			JsonNode reqData = searchRespone.path("request_data");
			
			JsonNode request_type = reqData.path("request_type");
			JsonNode entity_type = reqData.path("entity_type");
			JsonNode output_format = reqData.path("output_format");
			JsonNode value_delimiter = reqData.path("value_delimiter");
			JOptionPane.showMessageDialog(null, request_type.asText()+" "+entity_type.asText()+" "+output_format.asText()+" "+value_delimiter.asText());
		
		
		JsonNode app_data = searchRespone.path("app_data").path("project");
		JsonNode project = app_data.path("project");
		//JOptionPane.showMessageDialog(null, delRec.path("app_data").path("project"));
		
		/* ARRAY WAWADUKAN */
		for (JsonNode node1 : searchRespone.path("app_data").path("project")) {
			JsonNode type = node1.path("project_name");
			JOptionPane.showMessageDialog(null, type.path("display_name").asText());
			JOptionPane.showMessageDialog(null, type.path("value").asText());						
		}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}

}
