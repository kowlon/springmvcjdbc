package net.codejava.spring.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import net.codejava.spring.dao.ContactDAO;
import net.codejava.spring.model.Contact;
import net.codejava.spring.model.deleteJsonRequest;
import net.codejava.spring.model.modelJson;
import net.codejava.spring.request.requestInputJSON;
import net.codejava.spring.util.instantisUtilMain;

import org.json.JSONException;
import org.omg.CORBA.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller routes accesses to the application to the appropriate
 * hanlder methods. 
 * @author www.codejava.net
 *
 */
@Controller
public class HomeController {

	@Autowired
	private ContactDAO contactDAO;
	
	@Autowired
	ServletContext context; 
	
	@RequestMapping(value="/")
	public ModelAndView listContact(ModelAndView model) throws IOException, JSONException{
//		List<Contact> listContact = contactDAO.list();
//		
//				
//		instantisUtilMain main = new instantisUtilMain();
//		main.utilMain();
//		String[] search = main.utilMain().split("\\!");
//				
//		System.out.println("Project ID : "+search[0]);	
//		
//		model.addObject("project_id",search);
//		model.addObject("project_name",search[1]);
//		model.addObject("project_manager",search[2]);
		
//		model.addObject("projectSearch",search);
//		model.addObject("listContact", listContact);
//		model.addObject("jumlah", contactDAO.idCount());
		
		List<Contact> listContact = contactDAO.list();		
		model.addObject("listContact", listContact);
		model.addObject("jumlah", "100");
		model.addObject("namaUser", "Kowlon");
		model.addObject("jumlahProduk", "244");
		model.addObject("jumlahDepartment", "7");
		model.addObject("jumlahProject", "150");
		model.setViewName("index");
		
		
		
		model.setViewName("index");
		
		return model;
	}
	
	@RequestMapping(value="/new")
	public ModelAndView newcon(ModelAndView model) throws IOException{
		List<Contact> listContact = contactDAO.list();
		
		
		model.addObject("listContact", listContact);
		model.addObject("jumlah", contactDAO.idCount());
		model.setViewName("newContact");
		
		return model;
	}
	
	@RequestMapping(value="/index")
	public ModelAndView index(ModelAndView model) throws IOException, JSONException{
		String folderRes = "resources\\json";
		String uploadPath = context.getRealPath("") + folderRes;
		
		//Membuat Request Delete parameter untuk instantis API
		//Disimpan di resources/json agar bisa di akses via http
		
		//requestInputJSON req = new requestInputJSON();
		//req.createRequestDelete(uploadPath);
		
		//System.out.println(uploadPath);
		
		
		//System.out.println(f);
//		deleteJsonRequest delete = new deleteJsonRequest();
//		try {
//			delete.getJson(uploadPath);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		List<Contact> listContact = contactDAO.list();
		
		model.addObject("listContact", listContact);
		model.addObject("jumlah", "100");
		model.addObject("namaUser", "Kowlon");
		model.addObject("jumlahProduk", "244");
		model.addObject("jumlahDepartment", "7");
		model.addObject("jumlahProject", "150");
		model.setViewName("index");
		
		return model;
	}
				
	@RequestMapping(value = "/newContact", method = RequestMethod.GET)
	public ModelAndView newContact(ModelAndView model) {
		Contact newContact = new Contact();
		model.addObject("contact", newContact);
		model.setViewName("ContactForm");
		return model;
	}
	
	@RequestMapping(value = "/saveContact", method = RequestMethod.POST)
	public ModelAndView saveContact(@ModelAttribute Contact contact) {
		contactDAO.saveOrUpdate(contact);		
		return new ModelAndView("redirect:/index");
	}
	
	@RequestMapping(value = "/deleteContact", method = RequestMethod.GET)
	public ModelAndView deleteContact(HttpServletRequest request) {
		int contactId = Integer.parseInt(request.getParameter("id"));
		contactDAO.delete(contactId);
		return new ModelAndView("redirect:/index");
	}
	
	@RequestMapping(value = "/editContact", method = RequestMethod.GET)
	public ModelAndView editContact(HttpServletRequest request) {
		int contactId = Integer.parseInt(request.getParameter("id"));
		Contact contact = contactDAO.get(contactId);
		ModelAndView model = new ModelAndView("ContactForm");
		model.addObject("contact", contact);
		
		return model;
	}
	
	@RequestMapping(value = "/editContactAjax", method = RequestMethod.GET)
	public ModelAndView editContactAjax(HttpServletRequest request) {
		int contactId = Integer.parseInt(request.getParameter("id"));
		Contact contact = contactDAO.get(contactId);
		ModelAndView model = new ModelAndView("index");
		model.addObject("contactAjax","kowlon");		
		return model;
	}
	
	@RequestMapping("/ajax")
    public ModelAndView helloAjaxTest() {
        return new ModelAndView("ajax", "message", "Ajax Amsterdam..");
    }
 
    @RequestMapping(value = "/ajaxtest", method = RequestMethod.GET)
    public @ResponseBody
    String getTime() {
 
        Random rand = new Random();
        float r = rand.nextFloat() * 100;
        String result = "<br>Next # is <b>" + r + "</b>. Generated on <b>" + new Date().toString() + "</b><br> Jumlah Data : " + contactDAO.idCount() ;
        System.out.println("Debug Message from Controller :" + new Date().toString());
        return result;
    }
	
}
