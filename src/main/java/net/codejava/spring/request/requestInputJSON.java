package net.codejava.spring.request;

import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class requestInputJSON {
	
public String createRequestDelete(String location) throws JSONException{
		
		JSONObject globalArr = new JSONObject();
		globalArr.put("char_set", "UTF-8");
		globalArr.put("date_format", "MMM/dd/yyyy");
		globalArr.put("language", "en");
		globalArr.put("api_version", "15.1");
		
		JSONObject globalArr1 = new JSONObject();
		globalArr1.put("request_type", "delete");
		globalArr1.put("entity_type", "project");
		globalArr1.put("output_format", "json");
		globalArr1.put("value_delimiter", "|");
		
		JSONObject project_number1 = new JSONObject();
		project_number1.put("display_name", "Project ID");
		project_number1.put("value", "48");
		
		JSONObject project_name1 = new JSONObject();
		project_name1.put("display_name", "Project Name");
		project_name1.put("value", "projectForDelete");
		
		JSONObject delete_charter1 = new JSONObject();
		delete_charter1.put("display_name", "Delete Existing Charter");
		delete_charter1.put("value", "");
		
		JSONObject delete_idea1 = new JSONObject();
		delete_idea1.put("display_name", "Delete Existing Idea (if any)");
		delete_idea1.put("value", "");
		
		JSONObject projectObject = new JSONObject();
		projectObject.put("project_number", project_number1);
		projectObject.put("project_name", project_name1);
		projectObject.put("delete_charter", delete_charter1);
		projectObject.put("delete_idea", delete_idea1);
		
		JSONArray arrayProject1 = new JSONArray();
		arrayProject1.put(projectObject);
		
		JSONObject arrayProject = new JSONObject();
		arrayProject.put("project", arrayProject1);
			
		JSONObject global = new JSONObject();
		global.put("global_properties", globalArr);
		global.put("request_data", globalArr1);	
		global.put("app_data", arrayProject);	
		
				
		//try (FileWriter file = new FileWriter("D:\\xampp\\htdocs\\json\\test.json")) {
		
		try (FileWriter file = new FileWriter(location+"\\requestDelete.json")) {
			    //10.2.125.79
	            file.write(global.toString());	            
	            //System.out.println(global.toString());
	            file.flush();

	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		
		
		return global.toString();
		
	}

}
