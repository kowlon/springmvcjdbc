package net.codejava.spring.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

import org.json.JSONArray;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.codejava.spring.util.*;

public class instantisUtilMain {
	private String requestFormat = "";
	private String baseURL = "" ;
	private String loginId="",password="";
	private String responeJson ="";
	
	Loggering logger = new Loggering();
	

	public String generateJson(String json){
		String combo="";
		String combro="";
		try {			
			ObjectMapper mapper = new ObjectMapper();						
			JsonNode delRec = mapper.readTree(json);
			
			JsonNode globalPro = delRec.path("global_properties");
			boolean direct = delRec.path("app_data").path("project").isArray();
			JsonNode arraynode = delRec.path("app_data").path("project");
			boolean arracuy = delRec.isArray();
			//System.out.println("array : "+arraynode);
				for (JsonNode arrayProject : delRec.path("app_data").path("project")){
					JsonNode projNumber = arrayProject.path("project_number").path("value");
					JsonNode projManagerName = arrayProject.path("project_manager").path("value");
					JsonNode initiativeVal = arrayProject.path("pcs").path("value");
					JsonNode pNameValue = arrayProject.path("project_name").path("value");
					JsonNode pmnamevalue = arrayProject.path("primary_metric_name").path("value");
					
//					System.out.println("Project ID : "+projNumber.asText());
//					System.out.println("Project Name : "+pNameValue.asText());
//					System.out.println("Project Manager : "+projManagerName.asText());
//					System.out.println("Project Initiative : "+initiativeVal.asText());
//					System.out.println("Project Metric Name : "+pmnamevalue.asText());
					combo = projNumber.asText()+"|"+pNameValue.asText()+"|"+projManagerName.asText()+"|"+initiativeVal.asText()+"|"+pmnamevalue.asText()+"!";
					combro = combro.concat(combo);
					//System.out.println(combo);
					
					
								
				}
				
				System.out.println(combro);
				
							
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return combro;	
	}
	
	public String utilMain(){
			 	String responeJson=" ";
			 	String passing="";
				try {
						//System.out.println("fungsing di pangggil");
						CookieHandler.setDefault(new CookieManager(null,
						CookiePolicy.ACCEPT_ALL));	
						config config = new config();
											
						String accountName = config.accountName;
						String loginId = config.loginId;
						String password = config.password;
						String baseURL = config.baseURL;
						String dataFormat = config.dataFormat;
						String proxyServer = config.proxyServer;
						String proxyServerPort = config.proxyServerPort;
							
						System.setProperty("https.proxyHost",proxyServer);
						System.setProperty("https.proxyPort",proxyServerPort);
			
						instantisUtilMain objClient = new instantisUtilMain();
						
			
						if (dataFormat.equals("xml"))
							objClient.setrequestFormat(config.XML_FORMAT);
						if (dataFormat.equals("json"))
							objClient.setrequestFormat(config.JSON_FORMAT);
						
						logger.logMessage(objClient, "Setting attributes from properties");
						objClient.setloginId(loginId);
						objClient.setPassword(password);
						objClient.setbaseURL(baseURL+accountName);
						//objClient.integrate();
						//System.out.println(objClient.integrate());
						responeJson = objClient.integrate();
						instantisUtilMain respn = new instantisUtilMain();
						respn.setResponeJson(responeJson);
						passing = respn.generateJson(responeJson);
						
						
					} catch (Exception e) {
						logger.logError("ETrackClient", "Internal error ", e);
					}
				return passing;
	}
	
	public String integrate() {
		String returned = "";
		try {
			logger.logMessage(this,"ETrackClient Started.");
			config config = new config();
			//String inputQuery = config.inputQuery;
			String inputQuery = config.inputQuery;
			HttpClient client = new HttpClient(baseURL,loginId,password,requestFormat);

			logger.logMessage(this,"Login.");
			String loginResponse = client.doLogin();
			
			/* User can submit multiple request in the same session before calling doLogout. Here, we are calling only one request */
			String submitURL = ETrackUtils.getTagValue(loginResponse,"api_url");
			String responseCode = ETrackUtils.getTagValue(loginResponse,"result_code");
			if (!responseCode.equals("SUCCESS")) {
				logger.logMessage(this,"Authenticaion failed : " + loginResponse );
				System.out.println("Authenticaion failed : " + loginResponse) ;
				//return ;
			}
			logger.logMessage(this, "Submitting to "+submitURL) ;			
			String response = client.getResponse(submitURL,inputQuery);
			returned = response;
			
			
			//System.out.println(returned);
			
			
			logger.logMessage(this,"Logout.");
			client.doLogout();

			logger.logMessage(this, "Writing response to output.txt") ;
			
			if(requestFormat.equals(config.XML_FORMAT)) {
				//ETrackUtils.writeFile(ETrackUtils.formatXML(response), outputFile);
			} else {
				//ETrackUtils.writeFile(ETrackUtils.formatJSON(response), outputFile);
			}
			logger.logMessage(this,"ETrackClient (response) completed.");
			
			
		} catch (Exception e) {
			logger.logError(this,"Can't integrate. ", e);
		}
		
		return returned;
		
	}
	
	public String getResponeJson() {
		return responeJson;
	}

	public void setResponeJson(String responeJson) {
		this.responeJson = responeJson;
	}
	
	public String getrequestFormat() {
		return requestFormat;
	}
	public void setrequestFormat(String requestFormat) {
		this.requestFormat = requestFormat;
	}

	public String getbaseURL() {
		return baseURL;
	}

	public void setbaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public String getloginId() {
		return loginId;
	}

	public void setloginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
