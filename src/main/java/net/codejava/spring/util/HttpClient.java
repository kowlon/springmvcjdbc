package net.codejava.spring.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.net.URL;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


import net.codejava.spring.util.*;

public class HttpClient {
		
	public final static String lineSeparator = System.getProperty("line.separator");
	public final static String XML_FORMAT = "xml" ;
	public final static String JSON_FORMAT = "json" ;


	private String request_format = "";
	private String base_url = "" ;
	private String api_url = "" ;
	private String login_id="",password="";

	private HttpURLConnection httpCon = null;
	private HttpsURLConnection httpsCon = null;
	

	
	public HttpClient(String base_url,String login_id,String password, String request_format) {
		this.base_url = base_url ;
		this.login_id = login_id;
		this.password = password;
		this.request_format = request_format ;
	}
	
	public String doLogin() {
		api_url = base_url +"/integration/login";
	    String login_param1 = toURLParamater("user_login_id", login_id);
	    String pass_param1 = toURLParamater("password", password);
	    
		/* api_name value CRUD_API. */
	    String api_name1 = toURLParamater("api_name", "CRUD_API");
		
		/* output_type must be XML, as we parse response in XML format only. */
	    String output_type1 = toURLParamater("output_type", "xml");
		
		String urlParameters =  login_param1 + "&" + pass_param1 + "&" + api_name1 + "&" + output_type1;
		
		return getResponseInternal (api_url, urlParameters) ;
	}
	
	public String doLogout() {
		api_url = base_url +"/integration/logout";
		String urlParameters =  "" ;
		return getResponseInternal (api_url, urlParameters) ;
	}
	 	
	public String getResponse(String submitURL,String request_data) {

		String request_format1 = toURLParamater("request_format",this.request_format);
		String request_data1 = toURLParamater("request_data",request_data);

		String urlParameters =  request_data1+"&" + request_format1;

		return getResponseInternal (submitURL, urlParameters) ;
	}
	
	
	private String getResponseInternal (String url, String urlParameters) {
		String response = "";	
		if(url.contains("https")) {
			response = getResponseInternalHTTPS(url, urlParameters);
		} else {
			response = getResponseInternalHTTP(url, urlParameters);
		}
		return response;
	}
	
	private String getResponseInternalHTTP(String url, String urlParameters) {
		StringBuffer buff = new StringBuffer();
		OutputStream outputStream = null;
		InputStream inputStream = null;
		InputStreamReader ins = null;
		String line = "";
		try {
			URL httpurl = new URL(url);
			httpCon = (HttpURLConnection) (httpurl.openConnection());
			

			Loggering.logMessage(this, "Posting content to URL : " + url) ;
			setConnectionPropertiesHTTP(httpCon);				

			outputStream = httpCon.getOutputStream ();

			DataOutputStream wr = new DataOutputStream(outputStream);
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			inputStream = httpCon.getInputStream();

			ins = new InputStreamReader(inputStream);
			BufferedReader reader = new BufferedReader(ins);

			while ((line = reader.readLine()) != null) {
				buff = buff.append(line+lineSeparator);
			}
			Loggering.logMessage(this,	"Data retrieved.") ;
			
		} catch (Exception e) {
			Loggering.logError(this, "Error in getResponseInternal :"  + e.toString(),e);
		} finally {
			try {
				if(ins!=null) {
					ins.close();
				}
				if(inputStream!=null) {
					inputStream.close();
				}
				if(outputStream!=null) {
					outputStream.close();
				}
			} catch (IOException e) {
				Loggering.logError(this, "Error in getResponseInternal :"  + e.toString(),e);
			}
			urlParameters = "";
		}
		return buff.toString();
	}
	
	private String getResponseInternalHTTPS(String url, String urlParameters) {
		StringBuffer buff = new StringBuffer();
		OutputStream outputStream = null;
		InputStream inputStream = null;
		InputStreamReader ins = null;
		String line = "";
		try {
				URL httpsurl = new URL(url);
				
				httpsCon = (HttpsURLConnection) (httpsurl.openConnection());

				Loggering.logMessage(this, "Posting content to URL : " + url) ;
				setConnectionPropertiesHTTPS(httpsCon);				

				outputStream = httpsCon.getOutputStream ();

				DataOutputStream wr = new DataOutputStream(outputStream);
				wr.writeBytes(urlParameters);
				wr.flush();
				wr.close();

				inputStream = httpsCon.getInputStream();

				ins = new InputStreamReader(inputStream);
				BufferedReader reader = new BufferedReader(ins);

				while ((line = reader.readLine()) != null) {
					buff = buff.append(line+lineSeparator);
				}
				Loggering.logMessage(this,	"Data retrieved.") ;
				
			} catch (Exception e) {
				e.printStackTrace();
				Loggering.logError(this, "Error in getResponseInternal :"  + e.toString(),e);
			} finally {
				try {
					if(ins!=null) {
						ins.close();
					}
					if(inputStream!=null) {
						inputStream.close();
					}
					if(outputStream!=null) {
						outputStream.close();
					}
				} catch (IOException e) {
					Loggering.logError(this, "Error in getResponseInternal :"  + e.toString(),e);
				}
				urlParameters = "";
			}
			return buff.toString();
	}	
	private void setConnectionPropertiesHTTP(HttpURLConnection con) {
		try {
			/*
			 * Set connection properties for connection object passed.
			 */
			con.setConnectTimeout(600000);
			con.setReadTimeout(600000);
			con.setRequestMethod("POST");

			HttpURLConnection.setDefaultAllowUserInteraction(true);
			HttpURLConnection.setFollowRedirects(true);
			con.setInstanceFollowRedirects(true);
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setUseCaches(false);
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; ");			
			
		} catch (ProtocolException e) {
			Loggering.logError(this, "setConnectionProperties : " + e.getMessage() ,e);
		}

	}
	private void setConnectionPropertiesHTTPS(HttpsURLConnection con) {
		try {
			/*
			 * Set connection properties for connection object passed.
			 */
			HttpsURLConnection.setDefaultAllowUserInteraction(true);
			HttpsURLConnection.setFollowRedirects(true);

			
			con.setConnectTimeout(600000);
			con.setReadTimeout(600000);
			con.setRequestMethod("POST");

			con.setInstanceFollowRedirects(true);
			con.setDoOutput(true);
			con.setDoInput(true);
			con.setUseCaches(false);
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; ");			
			
		} catch (ProtocolException e) {
			Loggering.logError(this, "setConnectionProperties : " + e.getMessage() ,e);
		}

	}
	private String toURLParamater(String parameterName,String parameterValue) {
			String toURLParamater = "";
			try {
				toURLParamater = parameterName+"="+parameterValue;
				//URLEncoder.encode(toURLParamater, "UTF-8");
			} catch (Exception e) {
				Loggering.logError(this, "Encode error ", e);
			}
			
			return toURLParamater;
	}	
	
}
