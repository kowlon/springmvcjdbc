package net.codejava.spring.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import net.codejava.spring.util.*;



public class ETrackUtils {
	public final static String line_separator = System.getProperty("line.separator");

	public static String readFile(File inputFile) {
		return readFile(inputFile.getAbsolutePath());
	}

	public static String readFile(String filePath) {
		String fileContents="";
		FileReader fr = null;
		BufferedReader br = null;	    	
		try {
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);	    	
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(line_separator);
				line = br.readLine();
			}
			fileContents = sb.toString();
			
		} catch(FileNotFoundException ex) {
				Loggering.logError(new Object(), "Util.readFile : " + ex.getMessage() ,ex);
		} catch(Exception ex) {
				Loggering.logError(new Object(), "Util.readFile : " + ex.getMessage() ,ex);
		} finally {
			try {
				fr.close();
				br.close();
			} catch(IOException ex) {
				Loggering.logError(new Object(), "Util.readFile : " + ex.getMessage() ,ex);
			} catch(Exception ex) {
				Loggering.logError(new Object(), "Util.readFile : " + ex.getMessage() ,ex);
			}
		}
		return fileContents;
	}
	
	public static void writeFile(String fileContents,File inputFile) {
		writeFile(fileContents,inputFile.getAbsolutePath());
	}
	
	public static void writeFile(String fileContents,String filePath) {
			FileWriter fw = null;
			try {
				fw = new FileWriter(filePath);
				fw.write(fileContents);
			} catch(FileNotFoundException ex) {
					Loggering.logError(new Object(), "Util.writeFile : " + ex.getMessage() ,ex);
			} catch(Exception ex) {
					Loggering.logError(new Object(), "Util.writeFile : " + ex.getMessage() ,ex);
			} finally {
				try {
					fw.close();
				} catch(IOException ex) {
					Loggering.logError(new Object(), "Util.writeFile : " + ex.getMessage() ,ex);
				} catch(Exception ex) {
					Loggering.logError(new Object(), "Util.writeFile : " + ex.getMessage() ,ex);
				}
			}
	}
		
	
	public static String getTagValue (String strXML, String xmlTagName) {
		int startIndex = strXML.indexOf("<" + xmlTagName + ">"); 
		if (startIndex == -1)
			return "-1";
		
		
		int endIndex = strXML.indexOf("</" + xmlTagName + ">");
		if (endIndex == -1)
			return "-1" ;

		int tagLength = xmlTagName.length() ;
	
		String str = strXML.substring(startIndex + tagLength+2, endIndex).trim() ;

		return str ;
		
	}
	
	/*public static Document toDocument(String xml) {
		Document doc = null;
		InputSource inputSource = null;
		try {
			 DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			 DocumentBuilder docBuilder =  docFactory.newDocumentBuilder();
			 inputSource = new InputSource();
			 inputSource.setCharacterStream(new StringReader(xml));
			 doc = docBuilder.parse(inputSource);
		} catch (ParserConfigurationException e) {
			Logger.logError("ETrackUtils", "Util.toDocument : " + e.getMessage() ,e);
		} catch (SAXException e) {
			Logger.logError("ETrackUtils", "Util.toDocument : " + e.getMessage() ,e);			
		} catch (IOException e) {
			Logger.logError("ETrackUtils", "Util.toDocument : " + e.getMessage() ,e);			
		}
		return doc;
	}
	*/
	
	public static String formatXML(String input) {
		StreamResult xmlOutput = null;
		String result = input;
		StringReader stringReader = null;
		Source xmlInput = null;
		StringWriter stringWriter = null;
		Writer writer = null;
		try {
			if(!input.trim().equalsIgnoreCase("")) {
				input = input.replaceAll("\t"," ");
				input = input.replaceAll("> <","><");
				stringReader  = new StringReader(input.trim());
				xmlInput = new StreamSource(stringReader);
				stringWriter = new StringWriter();
				xmlOutput = new StreamResult(stringWriter);

				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
				transformer.transform(xmlInput, xmlOutput);
				writer = xmlOutput.getWriter();
				result = writer.toString();
				
			} else {
				result = input;	
			}
		}  catch (TransformerConfigurationException ex) {
			Loggering.logError(new Object(), "Util.formatXML : " + ex.getMessage() ,ex);
		}  catch (Exception ex) {
			Loggering.logError(new Object(), "Util.formatXML : " + ex.getMessage() ,ex);
			result = input;
		} finally {
			try {
				if(stringReader!=null) { 
					stringReader.close();
				}
				if(stringWriter!=null) {
					stringWriter.close();
				}
				if(writer!=null) {
					writer.close();
				}
			}
			catch (Exception ex) {
				Loggering.logError(new Object(), "Util.formatXML : " + ex.getMessage() ,ex);
				result = input;
			}	    	
		}
		
		return result;	    
	}
	
	public static String formatJSON(String input) {
			return input;
	}
	
}
