package net.codejava.spring.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Loggering {
	private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a");
	private static String ls = System.getProperty("line.separator");
	private final static String DEBUG_FILE_NAME = "debug_log.txt" ;
	private final static String ERROR_FILE_NAME = "error_log.txt" ;

	public static void logError(Object obj,String errorMessage,Exception ex) {
		String message = "";
		
	    StringWriter sw = new StringWriter();
	    PrintWriter pw = new PrintWriter(sw);
	    ex.printStackTrace(pw);
		
		String className = obj.getClass().getName() ;
		Date date = new Date() ;
		
		message = dateFormat.format(date)+"\t"+ className  + "\t" +"ERROR: "+"\t"+ errorMessage+ls;
		
		logMessage(ERROR_FILE_NAME,message);
		
		
	}
	
	public static void logMessage(Object obj,String errorMessage) {
		String message = "";
		
		String className = obj.getClass().getName() ;
		Date date = new Date() ;
		
		message = dateFormat.format(date)+"\t"+ className  + "\t" + errorMessage+ls;
		
		logMessage(DEBUG_FILE_NAME,message);
		
		
	}
	
	private static void logMessage (String file_name, String message) {
		RandomAccessFile raf = null;
		try {
			File f = new File(file_name);
			
			if(!f.exists()) {
				if(f.createNewFile()==false){
					throw new Exception("Unable to create log file.");
				}
			}
			
			raf = new RandomAccessFile(file_name, "rw");
			if(raf.skipBytes( (int)raf.length() )<(int)raf.length()){
				raf.close();
				throw new Exception("unable to append at the end, couldnt skip bytes.");
			}
		    raf.writeBytes(message);
		
		} catch(SecurityException ex) {
			throw new RuntimeException("Error in writing log file.", ex);
		} catch(IllegalArgumentException ex) {
			throw new RuntimeException("Error in writing log file.", ex);
		}  catch(FileNotFoundException ex) {
			throw new RuntimeException("Error in writing log file.", ex);
		} catch(IOException ex) {
			throw new RuntimeException("Error in writing log file.", ex);
		} catch(Exception ex) {
			throw new RuntimeException("Error in writing log file.", ex);
		} finally {
		    try {
		    	if(raf!=null) {
		    		raf.close();
		    	}
			} catch (IOException ex) {
								
			}			
		}
	
	}

}
