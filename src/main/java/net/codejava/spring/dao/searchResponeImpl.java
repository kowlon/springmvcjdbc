package net.codejava.spring.dao;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import net.codejava.spring.model.Contact;
import net.codejava.spring.model.responeSearch;
import net.codejava.spring.util.instantisUtilMain;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class searchResponeImpl implements searchRespone {

	@Override
	public List<responeSearch> list() {
		
		try {
			
		List<responeSearch> listContact = list();
		responeSearch respone = new responeSearch();	
		instantisUtilMain main = new instantisUtilMain();
		
		ObjectMapper mapper = new ObjectMapper();						
		JsonNode delRec = mapper.readTree(main.utilMain());
		JsonNode globalPro = delRec.path("global_properties");
		boolean direct = delRec.path("app_data").path("project").isArray();
		
		for (JsonNode arrayProject : delRec.path("app_data").path("project")){
			JsonNode projName = arrayProject.path("project_number").path("display_name");
			JsonNode projNumber = arrayProject.path("project_number").path("value");			
			JsonNode projManager = arrayProject.path("project_manager").path("display_name");
			JsonNode projManagerName = arrayProject.path("project_manager").path("value");
			JsonNode initiative = arrayProject.path("pcs").path("display_name");
			JsonNode initiativeVal = arrayProject.path("pcs").path("value");
			JsonNode pName = arrayProject.path("project_name").path("display_name");
			JsonNode pNameValue = arrayProject.path("project_name").path("value");
			JsonNode pmname = arrayProject.path("primary_metric_name").path("display_name");
			JsonNode pmnamevalue = arrayProject.path("primary_metric_name").path("value");
			
			respone.setProjectid(projNumber.asText());
			respone.setProjectmanager(projManagerName.asText());
			respone.setProjectname(pNameValue.asText());
			
			//rfp.arraylist().add(projName.asText());
			//rfp.arraylist().add(arrayProject.path("project_number").path("value").asText());
			
			//JOptionPane.showMessageDialog(null, projName.asText() +" "+ projNumber.asText());
			System.out.println("Project ID : "+projNumber.asText());
			System.out.println("Project Manager : "+projManagerName.asText());
			System.out.println("Project Initiative : "+initiativeVal.asText());
			System.out.println("Project Name : "+pNameValue.asText());
			System.out.println("Project Metric Name : "+pmnamevalue.asText());
			
		}
		
		
		
		
		
		
		
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		

		return null;
	}
	


}
