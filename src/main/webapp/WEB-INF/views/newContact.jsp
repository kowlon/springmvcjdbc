<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include page="../views/fragments/head.jsp" >
	<jsp:param name="userName" value="kowlon Doang" />
</jsp:include>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard 1</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        
                        <a href="newContact" target="" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add User</a>
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <a href="#" data-toggle="modal" data-target="#exampleModal" onclick="editContactAjax();">Edit</a>
                            <spring:url value="/resources/json/test.json" var="testJson" />
                            <a href="${testJson}">Test JSON</a>
                            
                            <li class="active"></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                
                 <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                	<div class="row">
						<div class="col-md-12">
							<div class="panel panel-info">
								<div class="panel-heading"> With two column</div>
								<div class="panel-wrapper collapse in" aria-expanded="true">
									<div class="panel-body">
										<form action="#">
											<div class="form-body">
												<h3 class="box-title">Person Info</h3>
												<hr>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">First Name</label>
															<input type="text" id="firstName" class="form-control" placeholder="John doe"> <span class="help-block"> This is inline help </span> </div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group has-error">
															<label class="control-label">Last Name</label>
															<input type="text" id="lastName" class="form-control" placeholder="12n"> <span class="help-block"> This field has error. </span> </div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Gender</label>
															<select class="form-control">
																<option value="">Male</option>
																<option value="">Female</option>
															</select> <span class="help-block"> Select your gender </span> </div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Date of Birth</label>
															<input type="text" class="form-control" placeholder="dd/mm/yyyy"> </div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Category</label>
															<select class="form-control" data-placeholder="Choose a Category" tabindex="1">
																<option value="Category 1">Category 1</option>
																<option value="Category 2">Category 2</option>
																<option value="Category 3">Category 5</option>
																<option value="Category 4">Category 4</option>
															</select>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Membership</label>
															<div class="radio-list">
																<label class="radio-inline p-0">
																	<div class="radio radio-info">
																		<input type="radio" name="radio" id="radio1" value="option1">
																		<label for="radio1">Option 1</label>
																	</div>
																</label>
																<label class="radio-inline">
																	<div class="radio radio-info">
																		<input type="radio" name="radio" id="radio2" value="option2">
																		<label for="radio2">Option 2 </label>
																	</div>
																</label>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<h3 class="box-title m-t-40">Address</h3>
												<hr>
												<div class="row">
													<div class="col-md-12 ">
														<div class="form-group">
															<label>Street</label>
															<input type="text" class="form-control"> </div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label>City</label>
															<input type="text" class="form-control"> </div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label>State</label>
															<input type="text" class="form-control"> </div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label>Post Code</label>
															<input type="text" class="form-control"> </div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label>Country</label>
															<select class="form-control">
																<option>--Select your Country--</option>
																<option>India</option>
																<option>Sri Lanka</option>
																<option>USA</option>
															</select>
														</div>
													</div>
													<!--/span-->
												</div>
											</div>
											<div class="form-actions">
												<button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
												<button type="button" class="btn btn-default">Cancel</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
                
                
                
                    <!-- /col-md-9 -->
                    <!-- col-md-3 -->
                    
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
                <!-- calendar widgets -->
                <!-- ============================================================== -->
                
                
                
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Kowlon  </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    
     <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="exampleModalLabel1">New message</h4> </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">${contactAjax}</label>
                                                    <input type="text" class="form-control" id="recipient-name1"> </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Message:</label>
                                                    <textarea class="form-control" id="message-text1"></textarea>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Send message</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
    <script type="text/javascript">
    function editContactAjax() {
        $.ajax({
            url : 'editContactAjax?id=${contact.id}',
            success : function(data) {
            	$('#contactAjax').html(data);
            }
        });
    }
</script>
    
   <jsp:include page="../views/fragments/foot.jsp" />