<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <spring:url value="/resources/plugins/bower_components/jquery/dist/jquery.min.js" var="jquery" />
    <script src="${jquery}"></script>
    <!-- Bootstrap Core JavaScript -->
    <spring:url value="/resources/bootstrap/dist/js/bootstrap.min.js" var="bootstrap" />
    <script src="${bootstrap}"></script>
    <!-- Menu Plugin JavaScript -->
    <spring:url value="/resources/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js" var="sidebarNav" />
    <script src="${sidebarNav}"></script>
    <!--slimscroll JavaScript -->
    <spring:url value="/resources/js/jquery.slimscroll.js" var="jquerySlim" />
    <script src="${jquerySlim}"></script>
    <!--Wave Effects -->
    <spring:url value="/resources/js/waves.js" var="waves" />
    <script src="${waves}"></script>
    <!--Counter js -->
    <spring:url value="/resources/plugins/bower_components/waypoints/lib/jquery.waypoints.js" var="waypoint" />
    <spring:url value="/resources/plugins/bower_components/counterup/jquery.counterup.min.js" var="counterup" />
    <script src="${waypoint}"></script>
    <script src="${counterup}"></script>
    <!--Morris JavaScript -->
    <spring:url value="/resources/plugins/bower_components/raphael/raphael-min.js" var="raphael" />
    <spring:url value="/resources/plugins/bower_components/morrisjs/morris.js" var="morris" />
    <script src="${raphael}"></script>
    <script src="${morris}"></script>
    <!-- chartist chart -->
    <spring:url value="/resources/plugins/bower_components/chartist-js/dist/chartist.min.js" var="chartist" />
    <spring:url value="/resources/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js" var="chartistTooltip" />
    <script src="${chartist}"></script>
    <script src="${chartistTooltip}"></script>
    <!-- Calendar JavaScript -->
    <spring:url value="/resources/plugins/bower_components/moment/moment.js" var="moment" />
    <spring:url value="/resources/plugins/bower_components/calendar/dist/fullcalendar.min.js" var="fullcalendar" />
    <spring:url value="/resources/plugins/bower_components/calendar/dist/cal-init.js" var="calInit" />
    <script src="${moment}"></script>
    <script src="${fullcalendar}"></script>
    <script src="${calInit}"></script>
    <!-- Custom Theme JavaScript -->
    <spring:url value="/resources/js/custom.min.js" var="custom" />
    <spring:url value="/resources/js/dashboard1.js" var="dashboard1" />
    <script src="${custom }"></script>
    <script src="${dashboard1 }"></script>
    <!-- Custom tab JavaScript -->
    <spring:url value="/resources/js/cbpFWTabs.js" var="cbpFWTabs" />
    <script src="${cbpFWTabs}"></script>
    <script type="text/javascript">
    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
    })();
    </script>
    <spring:url value="/resources/plugins/bower_components/toast-master/js/jquery.toast.js" var="jqueryToast" />
    <script src="${jqueryToast}"></script>
    <!--Style Switcher -->
    <spring:url value="/resources/plugins/bower_components/styleswitcher/jQuery.style.switcher.js" var="jquerySwitcher" />
    <!--  <script src="${jquerySwitcher}"></script>*/-->
</body>

</html>