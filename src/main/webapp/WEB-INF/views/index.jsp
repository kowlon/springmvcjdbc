<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include page="../views/fragments/head.jsp" >
	<jsp:param name="userName" value="kowlon Doang" />
</jsp:include>

        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Dashboard 1</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        
                        <a href="newContact" target="" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Add User</a>
                        <ol class="breadcrumb">
                            <li><a href="#">Dashboard</a></li>
                            <a href="#" data-toggle="modal" data-target="#exampleModal" onclick="editContactAjax();">Edit</a>
                            <spring:url value="/resources/json/test.json" var="testJson" />
                            <a href="${testJson}">Test JSON</a>
                            
                            <li class="active"></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                
                 <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-4 col-sm-12 row-in-br">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-danger"><i class="ti-clipboard"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15">${jumlahProduk}</h3></li>
                                        <li class="col-middle">
                                            <h4>Total Product</h4>
                                            
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-sm-12 row-in-br  b-r-none">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-info"><i class="fa fa-user"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15">${jumlah}</h3></li>
                                        <li class="col-middle">
                                            <h4>Total User</h4>
                                            
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-sm-12  b-0">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-warning"><i class="fa fa-industry"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15">${jumlahProject}</h3></li>
                                        <li class="col-middle">
                                            <h4>Total Projects</h4>
                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12 col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">MANAGE USERS</div>
                            <div class="table-responsive">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;" class="text-center">id</th>
                                            <th>NAME</th>
                                            <th>EMAIL</th>
                                            <th>ADDRESS</th>
                                            <th>PHONE</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="contact" items="${listContact}" varStatus="status">
                                        <tr>
                                            <td class="text-center">${status.index + 1}</td>
                                            <td><span class="font-medium">${contact.name}</span>
                                            </td>
                                            <td>${contact.email}</td>
                                            <td>${contact.address}</td>
                                            <td>${contact.telephone}</td>
                                            <td>
												<a href="editContact?id=${contact.id}">Edit</a>
												&nbsp;&nbsp;&nbsp;&nbsp;
												<a href="deleteContact?id=${contact.id}">Delete</a>
											</td>
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- /.row -->
               
                <!-- /.row -->
                <div class="row">
                    <div class="col-md-12 col-lg-8 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Products Yearly Sales</h3>
                            <ul class="list-inline text-right">
                                <li>
                                    <h5><i class="fa fa-circle m-r-5 text-info"></i>Mac</h5> </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5 text-danger"></i>Windows</h5> </li>
                            </ul>
                            <div id="ct-visits" style="height: 285px;"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 col-sm-12 col-xs-12">
                        <div class="bg-theme-alt">
                            <div id="ct-daily-sales" class="p-t-30" style="height: 300px"></div>
                        </div>
                        <div class="white-box">
                            <div class="row">
                                <div class="col-xs-8">
                                    <h2 class="m-b-0 font-medium">Week Sales</h2>
                                    <h5 class="text-muted m-t-0">Ios app - 160 sales</h5></div>
                                <div class="col-xs-4">
                                    <div class="circle circle-md bg-info pull-right m-t-10"><i class="ti-shopping-cart"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <div class="col-md-12 col-lg-8">
                        <div class="manage-users">
                            <div class="sttabs tabs-style-iconbox">
                                <nav>
                                    <ul>
                                        <li><a href="#section-iconbox-1" class="sticon ti-user"><span>Select Users</span></a></li>
                                        <li><a href="#section-iconbox-2" class="sticon ti-lock"><span>Set Permissions</span></a></li>
                                        <li><a href="#section-iconbox-3" class="sticon ti-receipt"><span>Message Users</span></a></li>
                                        <li><a href="#section-iconbox-4" class="sticon ti-check-box"><span>Save and finish</span></a></li>
                                    </ul>
                                </nav>
                                <div class="content-wrap">
                                    <section id="section-iconbox-1">
                                        <div class="p-20 row">
                                            <div class="col-sm-6">
                                                <h3 class="m-t-0">Select Users to Manage</h3></div>
                                            <div class="col-sm-6">
                                                <ul class="side-icon-text pull-right">
                                                    <li><a href="#"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Users</span></a></li>
                                                    <li><a href="#"><span class="circle circle-sm bg-danger di"><i class="ti-pencil-alt"></i></span><span>Edit</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="table-responsive manage-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th style="width: 150px;">NAME</th>
                                                        <th>POSITION</th>
                                                        <th>JOINED</th>
                                                        <th>LOCATION</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="advance-table-row active">
                                                        <td style="width: 10px;"></td>
                                                        <td style="width: 40px;">
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox7" checked="" type="checkbox">
                                                                <label for="checkbox7"> </label>
                                                            </div>
                                                        </td>
                                                        <td style="width: 60px;"><img src="/resources/plugins/images/users/varun.jpg" class="img-circle" alt="user img" width="30" /></td>
                                                        <td>Andrew Simons</td>
                                                        <td>Modulator</td>
                                                        <td>6 May 2016</td>
                                                        <td>Gujrat, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox8" type="checkbox">
                                                                <label for="checkbox8"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/genu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Hanna Gover</td>
                                                        <td>Admin</td>
                                                        <td>13 Jan 2005</td>
                                                        <td>Texas, United states</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox9" type="checkbox">
                                                                <label for="checkbox9"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/sonu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Nirav</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2001</td>
                                                        <td>Bhavnagar, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox10" type="checkbox">
                                                                <label for="checkbox10"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/pawandeep.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Sunil</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2004</td>
                                                        <td>Gujarat, India</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="p-10 p-t-30 row">
                                            <div class="col-sm-8">
                                                <h3>1 members selected</h3></div>
                                            <div class="col-sm-2 pull-right m-t-10"><a href="javascript:void(0);" class="btn btn-block p-10 btn-rounded btn-danger"><span>Next</span><i class="ti-arrow-right m-l-5"></i></a></div>
                                        </div>
                                    </section>
                                    <section id="section-iconbox-2">
                                        <div class="p-20 row">
                                            <div class="col-sm-6">
                                                <h3 class="m-t-0">Set Users Permission</h3></div>
                                            <div class="col-sm-6">
                                                <ul class="side-icon-text pull-right">
                                                    <li><a href="#"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Users</span></a></li>
                                                    <li><a href="#"><span class="circle circle-sm bg-danger di"><i class="ti-pencil-alt"></i></span><span>Edit</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="table-responsive manage-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>NAME</th>
                                                        <th>POSITION</th>
                                                        <th>JOINED</th>
                                                        <th>LOCATION</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="advance-table-row">
                                                        <td style="width: 10px;"></td>
                                                        <td style="width: 40px;">
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox25" type="checkbox">
                                                                <label for="checkbox25"> </label>
                                                            </div>
                                                        </td>
                                                        <td style="width: 60px;"><img src="/resources/plugins/images/users/varun.jpg" class="img-circle" alt="user img" width="30" /></td>
                                                        <td>Andrew Simons</td>
                                                        <td>Modulator</td>
                                                        <td>6 May 2016</td>
                                                        <td>Gujrat, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row active">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox12" checked="" type="checkbox">
                                                                <label for="checkbox12"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/genu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Hanna Gover</td>
                                                        <td>Admin</td>
                                                        <td>13 Jan 2005</td>
                                                        <td>Texas, United states</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox13" type="checkbox">
                                                                <label for="checkbox13"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/sonu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Nirav</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2001</td>
                                                        <td>Bhavnagar, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox14" type="checkbox">
                                                                <label for="checkbox14"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/pawandeep.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Sunil</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2004</td>
                                                        <td>Gujarat, India</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="p-10 row">
                                            <div class="col-sm-8">
                                                <h3>1 members selected</h3></div>
                                            <div class="col-sm-2 pull-right m-t-10"><a href="javascript:void(0);" class="btn btn-block p-10 btn-rounded btn-danger"><span>Next</span><i class="ti-arrow-right m-l-5"></i></a></div>
                                        </div>
                                    </section>
                                    <section id="section-iconbox-3">
                                        <div class="p-20 row">
                                            <div class="col-sm-6">
                                                <h3 class="m-t-0">Manage Users</h3></div>
                                            <div class="col-sm-6">
                                                <ul class="side-icon-text pull-right">
                                                    <li><a href="#"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Users</span></a></li>
                                                    <li><a href="#"><span class="circle circle-sm bg-danger di"><i class="ti-pencil-alt"></i></span><span>Edit</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="table-responsive manage-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>NAME</th>
                                                        <th>POSITION</th>
                                                        <th>JOINED</th>
                                                        <th>LOCATION</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="advance-table-row">
                                                        <td style="width: 10px;"></td>
                                                        <td style="width: 40px;">
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox16" type="checkbox">
                                                                <label for="checkbox16"> </label>
                                                            </div>
                                                        </td>
                                                        <td style="width: 60px;"><img src="/resources/plugins/images/users/varun.jpg" class="img-circle" alt="user img" width="30" /></td>
                                                        <td>Andrew Simons</td>
                                                        <td>Modulator</td>
                                                        <td>6 May 2016</td>
                                                        <td>Gujrat, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox15" type="checkbox">
                                                                <label for="checkbox15"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/genu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Hanna Gover</td>
                                                        <td>Admin</td>
                                                        <td>13 Jan 2005</td>
                                                        <td>Texas, United states</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row active">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox17" checked="" type="checkbox">
                                                                <label for="checkbox17"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/sonu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Nirav</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2001</td>
                                                        <td>Bhavnagar, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox18" type="checkbox">
                                                                <label for="checkbox18"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/pawandeep.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Sunil</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2004</td>
                                                        <td>Gujarat, India</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="p-10 row">
                                            <div class="col-sm-8">
                                                <h3>1 members selected</h3></div>
                                            <div class="col-sm-2 pull-right m-t-10"><a href="javascript:void(0);" class="btn btn-block p-10 btn-rounded btn-danger"><span>Next</span><i class="ti-arrow-right m-l-5"></i></a></div>
                                        </div>
                                    </section>
                                    <section id="section-iconbox-4">
                                        <div class="p-20 row">
                                            <div class="col-sm-6">
                                                <h3 class="m-t-0">Save and finish</h3></div>
                                            <div class="col-sm-6">
                                                <ul class="side-icon-text pull-right">
                                                    <li><a href="#"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Users</span></a></li>
                                                    <li><a href="#"><span class="circle circle-sm bg-danger di"><i class="ti-pencil-alt"></i></span><span>Edit</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="table-responsive manage-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>NAME</th>
                                                        <th>POSITION</th>
                                                        <th>JOINED</th>
                                                        <th>LOCATION</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="advance-table-row">
                                                        <td style="width: 10px;"></td>
                                                        <td style="width: 40px;">
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox19" type="checkbox">
                                                                <label for="checkbox19"> </label>
                                                            </div>
                                                        </td>
                                                        <td style="width: 60px;"><img src="/resources/plugins/images/users/varun.jpg" class="img-circle" alt="user img" width="30" /></td>
                                                        <td>Andrew Simons</td>
                                                        <td>Modulator</td>
                                                        <td>6 May 2016</td>
                                                        <td>Gujrat, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox20" type="checkbox">
                                                                <label for="checkbox20"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/genu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Hanna Gover</td>
                                                        <td>Admin</td>
                                                        <td>13 Jan 2005</td>
                                                        <td>Texas, United states</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox21" type="checkbox">
                                                                <label for="checkbox21"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/sonu.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Nirav</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2001</td>
                                                        <td>Bhavnagar, India</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" class="sm-pd"></td>
                                                    </tr>
                                                    <tr class="advance-table-row active">
                                                        <td></td>
                                                        <td>
                                                            <div class="checkbox checkbox-circle checkbox-info">
                                                                <input id="checkbox22" checked="" type="checkbox">
                                                                <label for="checkbox22"> </label>
                                                            </div>
                                                        </td>
                                                        <td><img src="/resources/plugins/images/users/pawandeep.jpg" alt="user img" class="img-circle" width="30" /></td>
                                                        <td>Joshi Sunil</td>
                                                        <td>Admin</td>
                                                        <td>21 Mar 2004</td>
                                                        <td>Gujarat, India</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="p-10 row">
                                            <div class="col-sm-8">
                                                <h3>1 members selected</h3></div>
                                            <div class="col-sm-2 pull-right m-t-10"><a href="javascript:void(0);" class="btn btn-block p-10 btn-rounded btn-danger"><span>Save</span><i class="ti-arrow-right m-l-5"></i></a></div>
                                        </div>
                                    </section>
                                </div>
                                <!-- /content -->
                            </div>
                            <!-- /tabs -->
                        </div>
                    </div>
                    <!-- /col-md-9 -->
                    <!-- col-md-3 -->
                    <div class="col-md-12 col-lg-4">
                        <div class="panel wallet-widgets">
                            <div class="panel-body">
                                <ul class="side-icon-text">
                                    <li class="m-0"><a href="#"><span class="circle circle-md bg-success di vm"><i class="ti-plus"></i></span><div class="di vm"><h1 class="m-b-0">$458.50</h1><h5 class="m-t-0">Your wallet Banalce</h5></div></a></li>
                                </ul>
                            </div>
                            <div id="morris-area-chart2" style="height:208px"></div>
                            <ul class="wallet-list">
                                <li><i class="icon-wallet"></i><a href="javascript:void(0)">Withdrow money</a></li>
                                <li><i class="icon-handbag"></i><a href="javascript:void(0)">Shop Now</a></li>
                                <li><i class="ti-archive"></i><a href="javascript:void(0)">Add funds</a></li>
                                <li><i class=" ti-wallet"></i><a href="javascript:void(0)">Withdrow money</a></li>
                                <li><i class="icon-wallet"></i><a href="javascript:void(0)">Withdrow money</a></li>
                                <li><i class="icon-handbag"></i><a href="javascript:void(0)">Shop Now</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
                <!-- calendar widgets -->
                <!-- ============================================================== -->
                
                
                
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Kowlon  </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    
     <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="exampleModalLabel1">New message</h4> </div>
                                        <div class="modal-body">
                                            <form>
                                                <div class="form-group">
                                                    <label for="recipient-name" class="control-label">${contactAjax}</label>
                                                    <input type="text" class="form-control" id="recipient-name1"> </div>
                                                <div class="form-group">
                                                    <label for="message-text" class="control-label">Message:</label>
                                                    <textarea class="form-control" id="message-text1"></textarea>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Send message</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
    <script type="text/javascript">
    function editContactAjax() {
        $.ajax({
            url : 'editContactAjax?id=${contact.id}',
            success : function(data) {
            	$('#contactAjax').html(data);
            }
        });
    }
</script>
    
   <jsp:include page="../views/fragments/foot.jsp" />